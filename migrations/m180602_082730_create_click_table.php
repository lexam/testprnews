<?php

use yii\db\Migration;

/**
 * Handles the creation of table `click`.
 */
class m180602_082730_create_click_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('click', [
            'id' => $this->string(32),
            'ua' => $this->string(255),
            'ip' => $this->string(255),
            'ref' => $this->string(255),
            'param1' => $this->string(255),
            'param2' => $this->string(255),
            'error' => $this->integer(11)->unsigned(),
            'bad_domain' => $this->integer(1),
        ]);
        $this->addPrimaryKey('click-index-id', 'click', 'id');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('click');
    }
}
