<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bad_domains`.
 */
class m180602_082806_create_bad_domains_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('bad_domains', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('bad_domains');
    }
}
