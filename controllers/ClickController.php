<?php

namespace app\controllers;

use app\models\BadDomains;
use Yii;
use app\models\Click;
use app\models\ClickSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ClickController implements the CRUD actions for Click model.
 */
class ClickController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Click models.
     * @return mixed
     */
    public function actionIndex($param1, $param2)
    {
        $ref = Yii::$app->request->referrer;
        $click  = Click::find()->where(['ref'=>$ref])->andWhere(['param1'=>$param1])->one();
        $badDomain = false;
        $arrBd = ArrayHelper::map(BadDomains::find()->select('id, name')->asArray()->all(), 'id', 'name');

        Yii::debug($arrBd);

        if(!empty($arrBd)){
           foreach ($arrBd as $value)
           {
               if(strpos($ref, $value)!==false){
                   $badDomain = true;
                   break;
               }
           }
        }
        if($click){
            $click->error++;
            $click->save();
            return $this->redirect(['error', 'id'=> $click->id]);
        }else{
            $click = new Click();
            $click->ua = Yii::$app->request->userAgent;
            $click->ip = Yii::$app->request->getUserIP();
            $click->ref = $ref;
            $click->param1 = $param1;
            $click->param2 = $param2;
            $click->error = 0;
            $click->bad_domain = 0;
            $click->save();
        }
        if($badDomain){

            Yii::$app->session->setFlash('badDomain');
            $click->bad_domain=1;
            $click->save();
            return $this->redirect(['error', 'id'=> $click->id]);
        }
        return $this->redirect(['success', 'id'=> $click->id]);
    }

    public function actionSuccess($id)
    {
        return $this->render('success');
    }
    public function actionError($id)
    {
        return $this->render('error');
    }


}
