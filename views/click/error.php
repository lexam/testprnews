<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = 'Error';
?>
<div class="site-error">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        This link is not valid!
    </div>



</div>
<?php if (Yii::$app->session->hasFlash('badDomain')): ?>
<script>
    window.addEventListener('load', function () {
        setTimeout(function () {
            location.href = "https://google.com/";
        }, 5000);
    }, false);
</script>
<?php endif;?>