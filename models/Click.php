<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "click".
 *
 * @property int $id
 * @property string $ua
 * @property string $ip
 * @property string $ref
 * @property string $param1
 * @property string $param2
 * @property int $error
 * @property int $bad_domain
 */
class Click extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'click';
    }

    public function beforeSave($insert)
    {
        if($insert){
            $this->id = $this->generateUniqueRandomString();
        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['error', 'bad_domain'], 'integer'],
            [['ua', 'ip', 'ref', 'param1', 'param2'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ua' => 'Ua',
            'ip' => 'Ip',
            'ref' => 'Ref',
            'param1' => 'Param1',
            'param2' => 'Param2',
            'error' => 'Error',
            'bad_domain' => 'Bad Domain',
        ];
    }

    public function generateUniqueRandomString($length = 32) {

        $attribute = 'id';
        $randomString = Yii::$app->getSecurity()->generateRandomString($length);

        if(!static::findOne([$attribute => $randomString]))
            return $randomString;
        else
            return $this->generateUniqueRandomString($attribute, $length);

    }
}
